import React from 'react';
import Nav from './components/Nav'
import Home from './views/Home'
import Schedule from './views/Schedule'
import EventNew from './views/events/New'
import EventEdit from './views/events/Edit'
import { BrowserRouter as Router, Route, Link, Redirect } from 'react-router-dom';
import { Container } from '@material-ui/core'
import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <Nav />
        <Container maxWidth='sm'>
          <Route path='/' component={Home} exact />
          <Route path='/events/new' component={EventNew} />
          <Route path='/events/:id/schedules/new' component={Schedule} />
          <Route path='/events/:id/edit' component={EventEdit} />
          <Route path='/404' component={Error} />
        </Container>
      </Router>
    </div>
  );
}

export default App;