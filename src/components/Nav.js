import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { AppBar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginBottom: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(2),
    flexGrow: 1,
  },
  navbar: {
    padding: theme.spacing(1),
  }
}));

export default function Nav() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position='static' className={classes.navbar}>
        <Typography variant='h6' className={classes.title}>ScheduleWith.friends</Typography>
      </AppBar>
    </div>
  )
}
